package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String getType() {
        return "シルド";
    }
    @Override
    public String defend(){
        return "defending using barrier";
    }
}
