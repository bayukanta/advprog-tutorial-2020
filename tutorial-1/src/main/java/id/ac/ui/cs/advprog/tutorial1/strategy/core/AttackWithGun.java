package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    @Override
    public String getType() {
        return "School Shooter";
    }
    @Override
    public String attack(){
            return "using gun pew-pew";
        }
}
