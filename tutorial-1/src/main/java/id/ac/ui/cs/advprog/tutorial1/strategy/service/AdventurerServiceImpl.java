package id.ac.ui.cs.advprog.tutorial1.strategy.service;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.stereotype.Service;

@Service
public class AdventurerServiceImpl implements AdventurerService {

    private final AdventurerRepository adventurerRepository;

    private final StrategyRepository strategyRepository;

    public AdventurerServiceImpl(AdventurerRepository adventurerRepository,
                                 StrategyRepository strategyRepository) {

        this.adventurerRepository = adventurerRepository;
        this.strategyRepository = strategyRepository;
    }

    @Override
    public Iterable<Adventurer> findAll() {
        return adventurerRepository.findAll();
    }

    @Override
    public Adventurer findByAlias(String alias) {
        return adventurerRepository.findByAlias(alias);
    }

    @Override
    public void changeStrategy(String alias, String attackType, String defenseType) {
        Adventurer petualang = this.findByAlias(alias);
        if (attackType.equals("School Shooter")){
            petualang.setAttackBehavior(new AttackWithGun());
        }
        else if(attackType.equals("Magic caster")){
            petualang.setAttackBehavior(new AttackWithMagic());
        }
        else{
            petualang.setAttackBehavior(new AttackWithSword());
        }
        if (defenseType.equals("Armor-armor")){
            petualang.setDefenseBehavior(new DefendWithArmor());
        }
        else if (defenseType.equals("Bari-Bari")){
            petualang.setDefenseBehavior(new DefendWithBarrier());
        }
        else{
            petualang.setDefenseBehavior(new DefendWithShield());
        }
    }

    @Override
    public Iterable<AttackBehavior> getAttackBehaviors() {
        return strategyRepository.getAttackBehaviors();
    }

    @Override
    public Iterable<DefenseBehavior> getDefenseBehaviors() {
        return strategyRepository.getDefenseBehaviors();
    }
}
