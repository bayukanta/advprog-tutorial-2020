package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    @Override
    public String getType() {
        return "Swordsman";
    }
    @Override
    public String attack(){
        return "using sword cring";
    }
}
